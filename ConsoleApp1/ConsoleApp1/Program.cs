﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
        // ---------The while loop
            //int num = 1;
            //while (num < 5)
            //{
            //    Console.WriteLine(num);
            //    num++;
            //}
            //Console.ReadLine();
        // ----------The do loop
            //int num = 0;
            //do
            //{
            //    Console.WriteLine(num);
            //    num++;
            //} while (num > 0 && num < 5);
            //Console.ReadLine();
        // The for loop
            //int i = 0;
            //for (i = 0; i < 5; i++)
            //    Console.WriteLine(i);
            //Console.ReadLine();
        // The for each loop
            ArrayList coName = new ArrayList();
            coName.Add("LucDT");
            coName.Add("Hue");
            coName.Add("Bang");

            foreach (string c in coName)
            {
                Console.WriteLine(c);
            }
            Console.ReadLine();
        }
    }
}
